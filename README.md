<p align="center">
<a href="https://gitee.com/CV_Lab/gradio-yolov8-det">
<img src="https://pycver.gitee.io/ows-pics/imgs/gradio_yolov8_det_logo.png" alt="Simple Icons" >
</a>
<p align="center">
    基于Gradio的YOLOv8通用目标检测与图像分割演示系统
</p>
<p align="center">
    可自定义检测模型、演示便捷、安装简单
</p>
</p>
<p align="center">
<a href="./CodeCheck.md"><img src="https://img.shields.io/badge/CodeCheck-passing-success" alt="code check" /></a>
<a href="https://gitee.com/CV_Lab/gradio-yolov8-det/releases/v0.2.4"><img src="https://img.shields.io/badge/Releases-v0.2.4-green" alt="Releases Version" /></a>
<a href="https://huggingface.co/"><img src="https://img.shields.io/badge/%F0%9F%A4%97-Hugging%20Face-blue" alt="Hugging Face Spaces" /></a>
<a href="https://huggingface.co/spaces"><img src="https://img.shields.io/badge/🤗%20Hugging%20Face-Spaces-blue" alt="Hugging Face Spaces" /></a>
<a href="https://gitee.com/CV_Lab/gradio-yolov8-det/blob/master/LICENSE"><img src="https://img.shields.io/badge/License-GPL--3.0-blue" alt="License" /></a>
</p>
<p align="center">
<a href="https://github.com/ultralytics/ultralytics"><img src="https://img.shields.io/badge/ultralytics-v8.0.152+-blue" alt="YOLOv8 Version" /></a>
<a href="https://github.com/gradio-app/gradio"><img src="https://img.shields.io/badge/Gradio-3.40.1+-orange" alt="Gradio Version" /></a>
<a href="#"><img src="https://img.shields.io/badge/Python-3.8%2B-blue?logo=python" alt="Python Version" /></a>
<a href="https://pypi.org/project/torch/"><img src="https://img.shields.io/badge/torch-2.0.1%2B-important?logo=pytorch" alt="Torch Version" /></a>
<a href="https://pypi.org/project/torchvision/"><img src="https://img.shields.io/badge/torchvision-0.15.2%2B-green?logo=pytorch" alt="TorchVision Version" /></a>
<a href="https://github.com/pre-commit/pre-commit"><img src="https://img.shields.io/badge/checks-pre--commit-brightgreen" alt="pre-commit"></a>
</p>

## 🚀 作者简介

曾逸夫，从事人工智能研究与开发；主研领域：计算机视觉；[YOLOv8官方开源项目代码贡献人](https://github.com/ultralytics/ultralytics/graphs/contributors)；[YOLOv5官方开源项目代码贡献人](https://github.com/ultralytics/yolov5/graphs/contributors)

❤️  Github：https://github.com/Zengyf-CVer

<h2 align="center">🚀更新走势</h2>

- `2023-08-14` **⚡ [Gradio YOLOv8 Det v0.2.4](https://gitee.com/CV_Lab/gradio-yolov8-det/releases/tag/v0.2.4)正式上线**
- `2023-04-14` **⚡ [Gradio YOLOv8 Det v0.2.3](https://gitee.com/CV_Lab/gradio-yolov8-det/releases/v0.2.3)正式上线**
- `2023-01-23` **⚡ [Gradio YOLOv8 Det v0.2.2](https://gitee.com/CV_Lab/gradio-yolov8-det/releases/v0.2.2)正式上线**
- `2023-01-22` **⚡ [Gradio YOLOv8 Det v0.2](https://gitee.com/CV_Lab/gradio-yolov8-det/releases/v0.2)正式上线**
- `2023-01-15` **⚡ [Gradio YOLOv8 Det v0.1](https://gitee.com/CV_Lab/gradio-yolov8-det/releases/v0.1)正式上线**

<h2 align="center">🤗在线Demo</h2>

### ❤️ 快速体验

本项目提供了**在线demo**，点击下面的logo，进入**Hugging Face Spaces**中快速体验：

<div align="center" >
<a href="https://huggingface.co/spaces/Zengyf-CVer/Gradio-YOLOv8-Det">
<img src="https://pycver.gitee.io/ows-pics/imgs/huggingface_logo.png">
</a>
</div>

<h2 align="center">💎项目流程与用途</h2>

### 📌 项目整体流程

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_8_2_workflow.png">
</div>

### 📌 项目示例

#### ❤️ Gradio YOLOv8 Det v0.2.4 界面与检测效果（目标检测）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_02_det.png">
</div>

#### ❤️ Gradio YOLOv8 Det v0.2.4 界面与检测效果（图像分割）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_02_2_seg.png">
</div>

#### ❤️ 快速体验

本项目提供了6个**图片示例**，用户可以快速体验检测与分割效果：

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_02_examples.png">
</div>
<div align="center" >
示例界面
</div>

<h2 align="center">💡项目结构</h2>

```
.
├── gradio-yolov8-det						# 项目名称
│   ├── model_config						# 模型配置
│   │   ├── model_name_all.yaml			    # YOLOv8 模型名称（yaml版）
│   │   └── model_name_custom.yaml  		# 自定义模型名称（yaml版）
│   ├── cls_name							# 类别名称
│   │   ├── cls_name_zh.yaml				# 类别名称文件（yaml版-中文）
│   │   ├── cls_name_en.yaml				# 类别名称文件（yaml版-英文）
│   │   ├── cls_name_ru.yaml				# 类别名称文件（yaml版-俄语）
│   │   ├── cls_name_es.yaml				# 类别名称文件（yaml版-西班牙语）
│   │   ├── cls_name_ar.yaml				# 类别名称文件（yaml版-阿拉伯语）
│   │   ├── cls_name_ko.yaml				# 类别名称文件（yaml版-韩语）
│   │   ├── cls_name.yaml					# 类别名称文件（yaml版-中文-v0.1）
│   │   └── cls_name.csv					# 类别名称文件（csv版-中文）
│   ├── util								# 工具包
│   │   ├── fonts_opt.py					# 字体管理
│   │   └── pdf_opt.py						# PDF管理
│   ├── img_examples						# 示例图片
│   ├── __init__.py							# 初始化文件
│   ├── gradio_yolov8_det.py			    # v0.2.4主运行文件
│   ├── gyd_fastapi_server.py			    # Gradio FastAPI运行文件
│   ├── setup.cfg							# pre-commit CI检查源配置文件
│   ├── .pre-commit-config.yaml				# pre-commit配置文件
│   ├── LICENSE								# 项目许可
│   ├── CodeCheck.md						# 代码检查
│   ├── .gitignore							# git忽略文件
│   ├── README.md							# 项目说明
│   └── requirements.txt					# 脚本依赖包
```

<h2 align="center">🔥安装教程</h2>

### ✅ 第一步：创建conda环境

```shell
conda create -n yolo python==3.8
conda activate yolo # 进入环境
```

### ✅ 第二步：克隆

```shell
git clone https://gitee.com/CV_Lab/gradio-yolov8-det.git
```

### ✅ 第三步：安装Gradio YOLOv8 Det依赖

```shell
cd gradio-yolov8-det
pip install -r ./requirements.txt -U
```

<h2 align="center">⚡使用教程</h2>

### 💡 运行Gradio YOLOv8 Det

📌 运行

```shell
python gradio_yolov8_det.py

# 在浏览器中输入：http://127.0.0.1:7860/或者http://127.0.0.1:7861/ 等等（具体观察shell提示）
```

❗ 注：默认类别文件[cls_name_zh.yaml](./cls_name/cls_name_zh.yaml)|[cls_name.csv](./cls_name/cls_name.csv)

### 💡 脚本指令操作

❤️ 本项目提供了一些脚本指令，旨在扩展项目的功能。

❗ 注：其中的一些功能是界面组件（按钮、文本框等）无法实现的，需要通过脚本指令完成：

```shell
# 共享模式
python gradio_yolov8_det.py -is # 在浏览器中以共享模式打开，https://**.gradio.app/

# 图片输入源切换，默认为图片上传
python gradio_yolov8_det.py -src upload # 图片上传
python gradio_yolov8_det.py -src webcam # webcam拍照

# 输入图片操作模式，默认为图片编辑器
python gradio_yolov8_det.py -it editor # 图片编辑器
python gradio_yolov8_det.py -it select # 区域选择

# 自定义下拉框默认模型名称
python gradio_yolov8_det.py -mn yolov8m

# 自定义NMS置信度阈值
python gradio_yolov8_det.py -conf 0.8

# 自定义NMS IoU阈值
python gradio_yolov8_det.py -iou 0.5

# 设置推理尺寸，默认为640
python gradio_yolov8_det.py -isz 320

# 设置滑块步长，默认为0.05
python gradio_yolov8_det.py -ss 0.01
```

### 💡 FastAPI 操作

❤️ 本项目可以内嵌于FastAPI框架

```shell
python gyd_fastapi_server.py
```

### 📝 项目引用指南

📌 如需引用Gradio YOLOv8 Det v0.2.4，请在相关文章的**参考文献**中加入下面文字：

```
曾逸夫, (2023) Gradio YOLOv8 Det (Version 0.2.4).https://gitee.com/CV_Lab/gradio-yolov8-det.git.
```

### 💬 技术交流

- 如果你发现任何Gradio YOLOv8 Det存在的问题或者是建议, 欢迎通过[Gitee Issues](https://gitee.com/CV_Lab/gradio-yolov8-det/issues)给我提issues。
- 欢迎加入CV Lab技术交流群

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/qq_group.jpg" width="20%">
</div>
